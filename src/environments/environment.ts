// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBQA3BbS9ccISy92Bag5uXOtaMSenzJbkE",
    authDomain: "controle-if2942.firebaseapp.com",
    databaseURL: "https://controle-if2942.firebaseio.com",
    projectId: "controle-if2942",
    storageBucket: "controle-if2942.appspot.com",
    messagingSenderId: "1070928197575",
    appId: "1:1070928197575:web:6de77471523c1d1a3fcbe2",
    measurementId: "G-4RRVEWYT97"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
